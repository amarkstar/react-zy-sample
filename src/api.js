import React, {Component} from 'react';
import {Route, Redirect} from 'react-router-dom';

const serverUrl = "http://121.120.241.13:90/api/";


export  default class Api{
    constructor(){
    }
	
	getList(name,params,callback){
		let url = serverUrl + name;
		if (params) {  
            for(let parm of params){
				url += '/' + parm;
			}
        }  
		
		fetch(url, {
			method: 'GET',
			credentials: "include",
			mode: 'cors',	
			headers: {
				"Content-Type": "application/json"
			  },			
		}).then((response) => {    
			
			if(response.status === 401){
				window.history.pushState(null,null,"#/login");
			}
			if (response.ok) {
				response.json().then(callback);
				; 
			}
        });
	};
	
	saveData(name,params,data,callback){
		let url = serverUrl + name;
		if (params) {  
            for(let parm of params){
				url += '/' + parm;
			}
        }  
		/*let formData = new FormData();  
		for(let key in data){
			formData.append(key,data[key]);  
		}*/
		
		fetch(url, {
			method: 'POST',
			credentials: "include",
			mode: 'cors',	
			headers: {
				"Content-Type": "application/json"
			  },	
			body:JSON.stringify(data),
		}).then((response) => {    
			
			if(response.status === 401){
				window.history.pushState(null,null,"#/login");
			}
			if (response.ok) {
				response.json().then(callback);
				; 
			}
        });
	};
	
}