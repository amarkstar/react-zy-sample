import React, { Component } from 'react';
import {
  Badge,
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  Table,
  Pagination,
  PaginationItem,
  PaginationLink,
  Button,
  Form,
  FormGroup,
  FormText,
  Label,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupButton
} from 'reactstrap';
import Api from "../../api";

class ManageProjects extends Component {
	constructor(props) {
    super(props);
    this.state = {
      project:[],
    };
	let api = new Api();
	api.getList("projects",[],(response)=>{
		console.log(response);
		this.setState({project:response});
	});
  }
  render() {
	  const rowsElements = [];
	  for (let item of this.state.project) {
		  rowsElements.push(
			<tr>
				<td>{item.ProjectCode}</td>
				<td>{item.Name}</td>
				<td>
					<Button onClick={() => this.props.history.push('/manage/project/'+ item.ProjectCode + '/edit')}><i className="fa fa-edit"></i>Edit</Button> &nbsp;
					<Button color="danger"><i className="fa fa-remove"></i>Delete</Button>
				</td>
			  </tr>
		  )
	  }
    return (
      <div className="animated fadeIn">		
        <Table responsive>
                  <thead>
                  <tr>
                    <th>Project Code</th>
                    <th>Name</th>
					<th></th>
                  </tr>
                  </thead>
                  <tbody>
				  {rowsElements}                  
                  </tbody>
                </Table>
				<Button color="primary"><i className="fa fa-new"></i>Create</Button>
      </div>
    )
  }
}

class ManageProject extends Component {
	constructor(props) {
    super(props);
    this.state = {
      project:{},
    };
	let api = new Api();
	const { project } = this.props.match.params;
	
	api.getList("projects",[project],(response)=>{		
		console.log(response);
		this.setState({project:response});
	});
  }
	render() {
		const rowsElements = [];
		if(this.state.project.ProjectPhases !== undefined){
		  for (let item of this.state.project.ProjectPhases) {
			  rowsElements.push(
				<tr>
					<td>{item.PhaseCode}</td>
					<td>{item.Name}</td>
					<td>
						<Button onClick={() => this.props.history.push('/manage/project/'+ this.state.project.ProjectCode + '/' + item.PhaseCode + '/edit')}><i className="fa fa-edit"></i>Edit</Button> &nbsp;
						<Button color="danger"><i className="fa fa-remove"></i>Delete</Button>
					</td>
				  </tr>
			  )
		  }
		};
		return (<div className="animated fadeIn">
			<FormGroup>
				<Label htmlFor="code">Code</Label> 
				<Input type="text" id="code" value={this.state.project.ProjectCode} readOnly  />
				<Label htmlFor="name">Name</Label> 
				<Input type="text" id="name" value={this.state.project.Name} />
				<br />
				<Button color="primary"><i className="fa fa-save"></i>&nbsp;Update</Button> &nbsp;
				<Button color="primary"><i className="fa fa-close"></i>&nbsp;Close</Button>
			</FormGroup>
			<div className="animated fadeIn">		
        <Table responsive>
                  <thead>
                  <tr>
                    <th>Phase Code</th>
                    <th>Name</th>
					<th></th>
                  </tr>
                  </thead>
                  <tbody>    
				  {rowsElements}				  
                  </tbody>
                </Table>
				<Button color="primary"><i className="fa fa-new"></i>Create</Button>
      </div>
      </div>)
	}
}

class ManagePhase extends Component {
	constructor(props) {
    super(props);
    this.state = {
      project:{},
    };
	let api = new Api();
	const { project,phase } = this.props.match.params;
	
	api.getList("projects",[project,phase],(response)=>{		
		console.log(response);
		this.setState({project:response});
	});
  }
	render() {
		const rowsElements = [];
		if(this.state.project.Blocks !== undefined){
		  for (let item of this.state.project.Blocks) {
			  rowsElements.push(
				<tr>
					<td>{item.Block}</td>
					<td>{item.Name}</td>
					<td>
						<Button onClick={() => this.props.history.push('/manage/project/'+ 
							this.state.project.ProjectCode + '/' + 
							this.state.project.PhaseCode + '/' + 
							item.Block + '/edit')}><i className="fa fa-edit"></i>Edit</Button> &nbsp;
						<Button color="danger"><i className="fa fa-remove"></i>Delete</Button>
					</td>
				  </tr>
			  )
		  }
		};
		return (<div className="animated fadeIn">
			<FormGroup>
				<Label htmlFor="code">Code</Label> 
				<Input type="text" id="code" value={this.state.project.PhaseCode} readOnly  />
				<Label htmlFor="name">Name</Label> 
				<Input type="text" id="name" value={this.state.project.Name} />
				<br />
				<Button color="primary"><i className="fa fa-save"></i>&nbsp;Update</Button> &nbsp;
				<Button color="primary"><i className="fa fa-close"></i>&nbsp;Close</Button>
			</FormGroup>
			<div className="animated fadeIn">		
        <Table responsive>
                  <thead>
                  <tr>
                    <th>Block Code</th>
                    <th>Name</th>
					<th></th>
                  </tr>
                  </thead>
                  <tbody>    
				  {rowsElements}				  
                  </tbody>
                </Table>
				<Button color="primary"><i className="fa fa-new"></i>Create</Button>
      </div>
      </div>)
	}
}

class ManageBlock extends Component {
	constructor(props) {
    super(props);
    this.state = {
      project:{},
	  agentGroup:[],
    };
	let api = new Api();
	const { project,phase,block } = this.props.match.params;
	
	api.getList("projects",[project,phase,block],(response)=>{		
		console.log(response);
		this.setState({project:response});
	});
	api.getList("agentgroup",[],(response)=>{		
		this.setState({agentGroup:response});
	});
	
  }
	render() {
		const agentHeader = [];
		for (let item of this.state.agentGroup) {
			agentHeader.push(
				<td>{item.Name}</td>
			);
		}
		
		const rowsElements = [];
		if(this.state.project.ProjectUnits !== undefined){
		  for (let item of this.state.project.ProjectUnits) {
			  const agentStaus = [];
			  for (let agent of this.state.agentGroup) {				  
				  agentStaus.push(
					<td><Input type="checkbox" id="checkbox1" name="checkbox1" checked={Array.isArray(item.Agents) && item.Agents.includes(agent.Id)} readOnly /></td>
				  );				  
			  }
			  
			  rowsElements.push(
				<tr>
					<td>{item.Row}</td>
					<td>{item.Column}</td>
					<td>{item.Name}</td>
					<td>{item.Code}</td>
					<td>{item.BuiltUpArea}&nbsp;{item.UnitMeasure}</td>
					<td>{item.LotStatus}</td>
						{agentStaus}
					<td>
						<Button onClick={() => this.props.history.push('/manage/project/'+ 
							this.state.project.ProjectCode + '/' + 
							this.state.project.PhaseCode + '/' + 
							this.state.project.Block + '/' + 
							item.Id + '/edit')}><i className="fa fa-edit"></i>Edit</Button> &nbsp;
						<Button color="danger"><i className="fa fa-remove"></i>Delete</Button>
					</td>
				  </tr>
			  )
		  }
		};
		return (<div className="animated fadeIn">
			<FormGroup>
				<Label htmlFor="code">Code</Label> 
				<Input type="text" id="code" value={this.state.project.Block} readOnly  />
				<Label htmlFor="name">Name</Label> 
				<Input type="text" id="name" value={this.state.project.Name} />
				<br />
				<Button color="primary"><i className="fa fa-save"></i>&nbsp;Update</Button> &nbsp;
				<Button color="primary"><i className="fa fa-close"></i>&nbsp;Close</Button>
			</FormGroup>
			<div className="animated fadeIn">		
        <Table responsive>
                  <thead>
                  <tr>
					<td>Level</td>
					<td>No</td>
					<td>Name</td>
					<td>Code</td>
					<td>BuiltUpArea</td>
					<td>LotStatus</td>
					{agentHeader}
					<th></th>
                  </tr>
                  </thead>
                  <tbody>    
				  {rowsElements}				  
                  </tbody>
                </Table>
				<Button color="primary"><i className="fa fa-new"></i>Create</Button>
      </div>
      </div>)
	}
}

class ManageUnit extends Component {
	constructor(props) {
    super(props);
    this.state = {
      project:{},
	  agentGroup:[],
    };
	let api = new Api();
	const { project,phase,block,unit } = this.props.match.params;
	
	api.getList("projects",[project,phase,block,unit],(response)=>{		
		console.log(response);
		this.setState({project:response});
	});
	
	api.getList("agentgroup",[],(response)=>{		
		this.setState({agentGroup:response});
	});
  }
  
	
	render() {		
	const agentControl = [];
		for (let item of this.state.agentGroup) {
			
			agentControl.push(
				<p><Label check htmlFor={"inline-checkbox" + item.Id}>
				  <Input type="checkbox" id={"inline-checkbox" + item.Id} name="inline-checkbox1" value={item.Id} checked={Array.isArray(this.state.project.Agents) && this.state.project.Agents.includes(item.Id)} onChange={(e)=>{this.handleCheckedChange(e)}} />{item.Name}
				 &nbsp;</Label></p>
				
			);
		}
		return (<div className="animated fadeIn">
			<FormGroup>
			
				<Label htmlFor="code">Code</Label> 
				<Input type="text" id="code" value={this.state.project.Unit} />
				<Label htmlFor="name">Name</Label> 
				<Input type="text" id="name" value={this.state.project.Name} />
				<Label htmlFor="row">Level</Label> 
				<Input type="text" id="row" value={this.state.project.Row} />				
				<Label htmlFor="column">No</Label> 
				<Input type="text" id="column" value={this.state.project.Column} />
				
					{agentControl}                        
                	
				<br />
				<Button color="primary" onClick={()=>{
                let api = new Api();
				const { project,phase,block,unit } = this.props.match.params;
	
				api.saveData("projects",[project,phase,block,unit],this.state.project,(response)=>{		
					console.log(response);
					this.setState({project:response});
				});
	
                }}>
				<i className="fa fa-save"></i>&nbsp;Update</Button> &nbsp;
				<Button color="primary"><i className="fa fa-close"></i>&nbsp;Close</Button>
			</FormGroup>			
      </div>)
	}
	
	
  handleCheckedChange(e){
	  var project = this.state.project;
	  var v = parseInt(e.target.value);
	  if(!Array.isArray(project.Agents))
			  project.Agents = [];
		  
	  if(e.target.checked){		  
		  if(!project.Agents.includes(v)){
			  project.Agents.push(v);
		  }
	  }
	  else{
		  if(project.Agents.includes(v)){
			  project.Agents.splice(project.Agents.indexOf(v),1);
		  }
	  }
	  
    this.setState({project:project});
  }
}

exports.ManageProjects = ManageProjects;
exports.ManageProject = ManageProject;
exports.ManagePhase = ManagePhase;
exports.ManageBlock = ManageBlock;
exports.ManageUnit = ManageUnit;


