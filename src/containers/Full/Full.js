import React, {Component} from 'react';
import {Link, Switch, Route, Redirect} from 'react-router-dom';
import {Container} from 'reactstrap';
import Header from '../../components/Header/';
import Sidebar from '../../components/Sidebar/';
import Breadcrumb from '../../components/Breadcrumb/';
import Aside from '../../components/Aside/';
import Footer from '../../components/Footer/';

import Login from '../../views/Login/'
import Auth from "../../auth";

import Dashboard from '../../views/Dashboard/';
import {ManageProjects,ManageProject,ManagePhase,ManageBlock,ManageUnit} from '../../views/ManageProject/'

let auth = new Auth();


class Full extends Component {
  render() {
    return (
      <div className="app">
        <Header />
        <div className="app-body">
          <Sidebar {...this.props}/>
          <main className="main">
            <Breadcrumb />
            <Container fluid>
              <Switch>
				<Route exact path="/login" name="Login Page" render={(props)=>{
                    return <Login auth={auth} {...props}/>
                }}/>
                <Route path="/dashboard" name="Dashboard" component={Dashboard}/>
				<Route path="/manage/project/list" name="ManageProjects" render={(props)=>{
                    if (auth.isLogin){
                        return <ManageProjects {...props}/>
                    }else{
                        return <Redirect to="/login"/>
                    }
                }}/>
				<Route path="/manage/project/:project/edit" name="ManageProject" render={(props)=>{
                    if (auth.isLogin){
                        return <ManageProject {...props}/>
                    }else{
                        return <Redirect to="/login"/>
                    }
                }}/>
				<Route path="/manage/project/:project/:phase/edit" name="ManagePhase" render={(props)=>{
                    if (auth.isLogin){
                        return <ManagePhase {...props}/>
                    }else{
                        return <Redirect to="/login"/>
                    }
                }}/>
				<Route path="/manage/project/:project/:phase/:block/edit" name="ManagePhase" render={(props)=>{
                    if (auth.isLogin){
                        return <ManageBlock {...props}/>
                    }else{
                        return <Redirect to="/login"/>
                    }
                }}/>
				<Route path="/manage/project/:project/:phase/:block/:unit" name="ManageUnit" render={(props)=>{
                    if (auth.isLogin){
                        return <ManageUnit {...props}/>
                    }else{
                        return <Redirect to="/login"/>
                    }
                }}/>
                <Redirect from="/" to="/dashboard"/>
              </Switch>
            </Container>
          </main>
          <Aside />
        </div>
        <Footer />
      </div>
    );
  }
}

export default Full;
