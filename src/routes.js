const routes = {
  '/': 'Home',
  '/dashboard': 'Dashboard',
  '/login': 'Login',
  '/manage/project':'Project',
  '/manage/project/:Code':':Code',
};
export default routes;
