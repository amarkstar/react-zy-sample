export  default class Auth{
    constructor(){
        this.isLogin = false; 
    }

    login(user_name,user_pwd,callback){
		let formData = new FormData();  
		formData.append("UserName",user_name);  
		formData.append("Password",user_pwd);  

		fetch('http://121.120.241.13:90/api/auth', {
            method: 'POST',
			//headers:new Headers({ 'Access-Control-Allow-Origin': 'http://localhost:3000'}),
			credentials: "include",
			mode: 'cors',
			body:formData,
        }).then((response) => {
            this.isLogin = true;
            callback(); 
        }).catch((err) => {
            console.error(err);
        });
    }
}